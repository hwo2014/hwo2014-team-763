# coding: utf-8
#

import json
import unittest
import functools

import mock
import numpy as np

from racebot import RaceBot, RaceAI, Race, Car, Track, Piece, Lane, Graph, MyCar

class TestRace(unittest.TestCase):
    def test_init(self):
        name = 'rcbot-1'
        data = json.load(open('testdata/keimola.json'))
        race = Race(data)
        self.failUnless('red' in race.cars.keys())
        self.assertEqual(name, race.cars['red'].name)
        self.assertEqual(0, race.tick)

class TestCar(unittest.TestCase):
    def test_update(self):
        data = json.load(open('testdata/keimola.json'))
        race = Race(data)
        data = [
            {'id': {'color': 'blue', 'name': 'rcbot-1'},
             'angle':-32.1,
             'piecePosition': {"pieceIndex":2,"inPieceDistance":12.304,"lane":{"startLaneIndex":1,"endLaneIndex":2},"lap":9}}
        ]
        
        car = Car(race, 'rcbot-1', 'blue')
        race.tick = 0
        car.update(data[0])
        self.assertEqual(-32.1, car.angle)
        self.assertEqual((1, 2), car.lane)
        self.assertEqual((2, 12.304), car.position)
        self.assertEqual(9, car.lap)
        self.assertEqual(112.304, car.speed)

        data = [
            {'id': {'color': 'blue', 'name': 'rcbot-1'},
             'angle':-32.1,
             'piecePosition': {"pieceIndex":2,"inPieceDistance":22.304,"lane":{"startLaneIndex":1,"endLaneIndex":2},"lap":9}}
        ]
        race.tick = 1
        car.update(data[0])
        self.assertAlmostEqual(9.9999, car.speed, places=2)
        self.assertEqual(-102.304, car.acceleration)

    def test_init(self):
        car = Car(None, 'rcbot-1', 'red')
        self.assertEqual(0, car.lap)
        self.assertEqual((0, 0.0), car.position)
        self.assertEqual((0, 0), car.lane)
        self.assertEqual(0, car.speed)
        self.assertEqual(0, car.throttle)
        self.assertEqual(0, car.acceleration)

    def test__compute_speed(self):
        data = json.load(open('testdata/keimola.json'))
        race = Race(data)
        car = Car(race, 'rcbot-1', 'blue')
        car._position.append((0, 0))
        self.assertEqual(0, car._compute_speed())
        
        car._position.append((0, 50))
        self.assertEqual(50, car._compute_speed())
        
        car._position.append((1, 2.5))
        self.assertEqual(52.5, car._compute_speed())
        
        car._position.clear()
        car._position.append((5, 0))
        car._position.append((5, 10))
        self.assertEqual(10.0, car._compute_speed())


class TestTrack(unittest.TestCase):
    def test_init(self):
        data = json.load(open('testdata/keimola.json'))
        track = Track(data['race']['track'])
        self.assertEqual(40, len(track.pieces))
        self.assertEqual(2, len(track.lanes))

    def test_get_lane_length(self):
        data = json.load(open('testdata/keimola.json'))
        track = Track(data['race']['track'])
        self.assertEqual(100, track.get_lane_length(0, 0))
        self.assertEqual(100, track.get_lane_length(0, 1))
        self.assertAlmostEqual(86.393, track.get_lane_length(5, 0), places=2)
        self.assertAlmostEqual(70.686, track.get_lane_length(5, 1), places=2)
        
    def test_compute_distance(self):
        data = json.load(open('testdata/keimola.json'))
        track = Track(data['race']['track'])
        p = [(0,0,0), (0,50,0)]
        self.assertEqual(50, track.compute_distance(p[0], p[1]))

        p = [(0,0,0), (1,0,0)]
        self.assertEqual(100, track.compute_distance(p[0], p[1]))

        p = [(29,67.585,0), (30,1.899,0)]
        self.assertAlmostEqual(4.999, track.compute_distance(p[0], p[1]), places=2)

        p = [(5,0,0), (5,50,0)]
        self.assertEqual(50, track.compute_distance(p[0], p[1]))
        
        p = [(5,0,0), (5,50,1)]
        self.assertEqual(50, track.compute_distance(p[0], p[1]))

        p = [(30,70.464,0), (31,4.61,0)]
        self.assertAlmostEqual(4.8318, track.compute_distance(p[0], p[1]), places=2) # 4.999

        p = [(4,85.143,0), (5, 6.064,0)]
        self.assertAlmostEqual(7.31479, track.compute_distance(p[0], p[1]), places=2)
        
    def test_compute_shortest_path(self):
        data = json.load(open('testdata/keimola.json'))
        track = Track(data['race']['track'])
        path = track.shortest_path_lanes(1)
        self.assertEqual(40, len(path.keys()))
        print 'path', [ (p,l) for p,l in path.items()]
        print 'switch path', [ (p,l) for p,l in path.items() if track.pieces[p].has_switch()]
        dist = sum([track.get_lane_length(p,l) for p,l in path.iteritems()])
        
        # self.assertAlmostEqual(3174.1237, dist, places=2)
    def test_longest_straight(self):
        data = json.load(open('testdata/keimola.json'))
        track = Track(data['race']['track'])
        path = track.get_longest_straight()
        self.assertEqual((35, 490.0), path)
        
    def test_compute_shortest_path_2(self):
        data = json.load(open('testdata/qualification_england_dnf.json'))
        track = Track(data['race']['track'])
        path = track.shortest_path_lanes(1)
        self.assertEqual(64, len(path.keys()))

class TestMyCar(unittest.TestCase):
    def test_init(self):
        data = json.load(open('testdata/keimola.json'))
        race = Race(data)
        car = MyCar(race, mock.Mock(), mock.Mock())
    def test_mass(self):
        data = json.load(open('testdata/keimola.json'))
        race = Race(data)
        car = MyCar(race, mock.Mock(), mock.Mock())
        self.assertEqual(6.24, car._approx_mass())
        car._speed.append(1.0)
        car._speed.append(2.2)
        car._speed.append(3.0)
        self.assertEqual(6.20, car._approx_mass())

    def test__find_next_bend(self):
        data = json.load(open('testdata/qualification_england_dnf.json'))
        race = Race(data)
        car = MyCar(race, mock.Mock(), mock.Mock())
        self.failUnless(car._find_next_bend())
        print len(race.track.pieces)
        print car._path
        car._position = [(29, 0)]
        print car._find_next_bend()
        self.failUnless(car._find_next_bend())

class TestLane(unittest.TestCase):
    def test_init(self):
        data = {"distanceFromCenter":-10,"index":1}
        lane = Lane(data)
        self.assertEqual(1, lane.index)
        self.assertEqual(-10, lane.distance)


class TestPiece(unittest.TestCase):
    def test_init(self):
        p = Piece({"length":100.0})
        self.assertEqual(100.0, p.length)
        self.assertEqual(True, p.is_straight())

        p = Piece({"length":100.0,"switch":True})
        self.assertEqual(100.0, p.length)
        self.assertEqual(True, p.is_straight())
        self.assertEqual(True, p.has_switch())

        p = Piece({"radius":100,"angle":45.0})
        self.assertEqual(45.0, p.angle)
        self.assertEqual(100.0, p.radius)
        self.assertEqual(False, p.is_straight())

class TestRaceBot(unittest.TestCase):
    def test_on_car_positions(self):
        name = 'rcbot-1'
        bot = RaceBot(mock.Mock(), name, None)
        bot.send_telemetry = False
        bot.race = mock.Mock()
        bot.race.tick = 2
        bot.race.cars = {'red': Car(bot.race, 'rcbot-1', 'red')}
        bot.race.track.compute_distance.return_value = 1.0
        bot.race.track.pieces = [Piece({'length': 100.0})]
        data = [{"id":{"name":"rcbot-1","color":"red"},"angle":3.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":1.1840800000000002,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}]
        bot.on_car_positions(data)

    def test_on_game_init(self):
        name = 'rcbot-1'
        bot = RaceBot(mock.Mock(), name, None)
        bot.send_telemetry = False
        bot.race = mock.Mock()
        data = {'race': {'track': {"id":"keimola","name":"Keimola","pieces":[], "lanes": []},"cars":[]}}
        bot.on_game_init(data)
        bot.on_game_init(data)

class TestGraph(unittest.TestCase):
    def test_add_edge(self):
        g = Graph()
        g.add_edge(1, 2)
        self.assertEqual(1, len(g.edges(1)))
        self.assertEqual((2, 0.0), g.edges(1)[0])
        g.add_edge(1, 2)
        self.assertEqual(1, len(g.edges(1)))
        g.add_edge(1, 3, 1.5)
        self.assertEqual(2, len(g.edges(1)))
        self.assertEqual((3, 1.5), g.edges(1)[1])
        
    def test_compute_shortest_path(self):
        g = Graph()
        g.add_edge(1, 2, 1.0)
        g.add_edge(1, 3, 2.0)
        g.add_edge(2, 4, 1.0)
        g.add_edge(3, 4, 1.0)
        path = g.shortest_path(1, 4)
        self.assertEqual([1,2,4], path)

    def test_compute_shortest_path2(self):        
        g = Graph()
        g.add_edge(1, 2, 2.0)
        g.add_edge(1, 3, 1.0)
        g.add_edge(2, 4, 2.0)
        g.add_edge(2, 5, 2.0)
        g.add_edge(3, 4, 1.0)
        g.add_edge(3, 5, 2.0)
        g.add_edge(4, 6, 2.0)
        g.add_edge(4, 7, 1.0)
        g.add_edge(5, 6, 2.0)
        g.add_edge(5, 7, 2.0)
        g.add_edge(6, 8, 1.0)
        g.add_edge(7, 8, 1.0)
        path = g.shortest_path(1, 8)
        self.assertEqual([1,3,4,7,8], path)

        

