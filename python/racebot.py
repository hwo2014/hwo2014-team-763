# coding: utf-8

import os
import pickle
import json
import time
import shutil
import random
from collections import deque
import csv


import numpy as np

DEBUG = os.environ.get('HWODEBUG', 'false').lower() == 'true'

import logging
import logging.config
logger = logging.getLogger('racecondition.racebot')
logging.config.fileConfig('logging.ini')

    


class Telemetry(object):
    def __init__(self, gameId):
        try:
            os.makedirs('telemetry')
        except: pass
        self.gameId = gameId
        fname = 'telemetry/game-%s.csv' % (self.gameId, )
        f = open(fname, 'wb')
        self.csvwriter = csv.writer(f)
        logger.info('[TELEMETRY] initialized for game: %s' % (self.gameId, ))
        self.header = None
    
    def send(self, msg):
        if not self.header:
            self.header = msg.keys()
            self.csvwriter.writerow(self.header)
        self.csvwriter.writerow([msg[k] for k in self.header])


class Car(object):
    
    def __init__(self, race, name, color):
        self.name = name
        self.color = color
        self.race = race
        self.finished = False
        
        self.reset()
    
    def reset(self):
        self._crashed = False
        self._last_tick = 0
        self._turbo = None  # turboAvailable
        self.turbo_available = None
        self._should_turbo = False
        self.finished = False

        # keep _hist_size observations of car state
        self._hist_size = 10
        
        self._lap = deque([0], maxlen=self._hist_size)
        self._angle = deque([0], maxlen=self._hist_size)
        self._position = deque([(0, 0)], maxlen=self._hist_size)
        self._lane = deque([(0, 0)], maxlen=self._hist_size)
        self._speed = deque([0], maxlen=self._hist_size)
        self._acceleration = deque([0], maxlen=self._hist_size)
        self._throttle = deque([0], maxlen=self._hist_size)
        self._tick = deque([0], maxlen=self._hist_size)
        
        self._lane_switch = []
        self._last_switch = None
        
        self.next_bend = None
        self.next_bend_dist = None
        self.next_bend_braking_dist = None
        self.target_speed = None
        self.next_piece_dist = None
        self.next_piece = None
    
    def set_crashed(self, value):
        self._speed.append(0)
        self._tick.append(self.race.tick)
        self._crashed = value
    
    def get_crashed(self):
        return self._crashed
    
    crashed = property(get_crashed, set_crashed)
    
    @property
    def lap(self):
        """Returns most recent lap number"""
        return self._lap[-1]
    
    @property
    def speed(self):
        """Returns most recent speed"""
        return self._speed[-1]
    
    @property
    def position(self):
        """Returns most recent position"""
        return self._position[-1]
    
    @property
    def angle(self):
        """Returns most recent angle"""
        return self._angle[-1]
    
    @property
    def lane(self):
        """Returns most recent lane"""
        return self._lane[-1]
    
    @property
    def throttle(self):
        """Returns most recent throttle"""
        return self._throttle[-1]

    @property
    def prev_throttle(self):
        """Returns second most recent throttle"""
        if len(self._throttle) > 1:
            return self._throttle[-2]
        return None
    
    def _compute_speed(self):
        if len(self._position) == 0:
            return 0
        p = (self._position[-2], self._position[-1])
        lane_idx = self.lane[0]
        dist = self.race.track.compute_distance(p[0] + (self.lane[0],), p[1] + (self.lane[0],))
        if len(self._tick) > 1:
            dt = (self._tick[-1] - self._tick[-2]) or 1.0
        else:
            dt = 1.0
        return dist / dt
    
    def _update_throttle(self):
        self._throttle.append(0.5)
    
    @property
    def tick(self):
        return self._tick[-1]
    
    @property
    def acceleration(self):
        return self._acceleration[-1]
    
    def _compute_acceleration(self):
        if len(self._tick) < 2:
            return 0.0
        dt = (self._tick[-1] - self._tick[-2])
        if dt == 0:
            return 0.0
        dv = self._speed[-1] - self._speed[-2]
        return dv / dt
    
    @property
    def force(self):
        return 0.0
    
    @property
    def angular_velocity(self):
        return 0.0
    
    @property
    def angular_speed(self):
        return 0.0
    
    def piece_index(self, rel):
        p = self._position[-1]
        i = (p[0] + rel) % len(self.race.track.pieces)
        return i

    def on_game_reset(self):
        self.reset()
    
    def piece(self, rel):
        p = self._position[-1]
        i = (p[0] + rel) % len(self.race.track.pieces)
        piece = self.race.track.pieces[i]
        return piece
    
    def on_lap_finished(self):
        self._last_switch = None
        pass
    
    def _update_lane(self):
        pass
    
    def get_next_lane(self):
        if len(self._lane_switch) > 0:
            return self._lane_switch.pop(0)
        return None

    def use_turbo(self):
        self.turbo_available = None
        
    def should_turbo(self):
        return False

    def _update_turbo(self):
        pass

    def update(self, data):
        """Updates Car state"""
        if self._last_tick and self._last_tick == self.race.tick:
            print 'car state for tick already updated'
            return
        if self.crashed:
            return
        
        piece_position = data['piecePosition']
        self._tick.append(self.race.tick)
        self._angle.append(data['angle'])

        lane = piece_position['lane']
        self._lane.append((lane['startLaneIndex'], lane['endLaneIndex']))
        
        self._position.append((piece_position['pieceIndex'],
            piece_position['inPieceDistance']))
        
        if piece_position.get('lap') >= 0:
            self._lap.append(piece_position['lap'])
        self._speed.append(self._compute_speed())
        self._acceleration.append(self._compute_acceleration())
        self._update_throttle()
        self._update_lane()
        self._update_turbo()
        if len(self._lap) > 1 and self._lap[-1] != self._lap[-2]:
            self.on_lap_finished()


class CruiceControl(object):
    def set_speed(self, value):
        pass

class MyCar(Car):
    
    def __init__(self, *args, **kw):
        super(MyCar, self).__init__(*args, **kw)
        self.reset()
        self._slope = 450.0
        self._max_angle = 0.0
        self._next_bend = [0, 0]
        self._dist_to_bend = 0
        self._max_accel = 0
        self._min_accel = 0
        self._deaccel_ratio = 0.975
        self._bend_speed = 0
        self._bend_accel = 0
        self._lane_updated = None
        
        self.next_bend = None
        self.next_bend_dist = None
        self.next_bend_braking_dist = None
        self.target_speed = None
        self.next_piece_dist = None
        self.next_piece = None
        
        self._inc_hold = 100.0
        self._dec_hold = 100.0
        
        self.turbo_available = None
        self._should_turbo = False
        self._turbo_timing = None
        self._longest_straight = None
        self._longest_straight = self.race.track.get_longest_straight()
        self._turbo_time = 0
        
        self._target_speed_factor = {}
        
        self._bend_speed_factors = {}
        self._last_switch = None
        self._lane_switching = []
        self._path = self.race.track.shortest_path_lanes()
        self._mass = None
        self._accel = None
        self._brake = None
        
        self._magic_bend_constant = 0.39
    
    def reset(self):
        super(MyCar, self).reset()
        self._bend_data = deque([(0,0,0)], 128)
        self._longest_straight = self.race.track.get_longest_straight()
    
    @property
    def force(self):
        return self.compute_force()

    def use_turbo(self):
        if not self.turbo_available:
            return
        logger.debug('Turbo used!')
        self._turbo_time = self.turbo_available.get("turboDurationTicks", 0)
        self.turbo_available = None

    def should_turbo(self):
        if not self.turbo_available:
            return False
        if not self._longest_straight:
            return False
        if self.piece(0).index != self._longest_straight[0] and \
            self.piece(-1).index != self._longest_straight[0] and \
            self.piece(-2).index != self._longest_straight[0]:
            return False
        if abs(self.angle) > 10:
            logger.debug('not using turbo. angle to great: %f' % (self.angle, ))
            return False
        braking_factor = self._approx_braking_factor()
        v0 = self.speed * float(self.turbo_available["turboFactor"])/2.0
        n = sum((braking_factor ** np.arange(0, 400) * v0) >= 6.9) # ticks
        braking_dist = sum(braking_factor ** np.arange(0, n + 1) * v0)
        #print 'turbo n:%7.3f next_bend:%7.3f braking_dist:%7.3f turboticks:%d' % (n, self.next_bend_dist, braking_dist, self.turbo_available["turboDurationTicks"])
        if braking_dist <= self.next_bend_dist:
            logger.debug('turbo!')
            return True
        else:
            logger.debug('not using turbo. braking distance insufficient.')
        return False
    
    def on_lap_finished(self):
        logger.info('lap finished')
        super(MyCar, self).on_lap_finished()
        self._path = self.race.track.shortest_path_lanes()
        if abs(self._max_angle) < 30:
            self._slope *= 0.95
            logger.info('[ADJUST] magic bend constant * 1.1')
            self._magic_bend_constant *= 1.1
        elif abs(self._max_angle) > 55:
            logger.info('[ADJUST] magic bend constant * 0.98')
            self._magic_bend_constant *= 0.98
    
    def compute_force(self, piece=None, speed=None):
        piece = piece or self.piece(0)
        v = speed or self.speed
        m = 1.0
        r = piece.angle or 0.0
        force = 0.0
        
        if abs(r) >= 1:
            force = m * v**2 / r * np.log(abs(0.1+self.angle))
        return force
    
    @property
    def angular_speed(self):
        if not self.piece(0).radius:
            return 0.0
        pos2 = self._position[-1] + (0,)
        pos1 = self._position[-2] + (0,)
        if pos1[0] != pos2[0]:
            return 0.0
        d = self.race.track.compute_distance(pos1, pos2)
        return d / float(self.piece(0).radius)
    
    def _update_lane(self):
        switch = None
        for i in range(0, 25):
            piece = self.piece(i)
            if piece.has_switch():
                switch = piece
                break
        if not switch:
            return
        if self._last_switch >= switch.index:
            return
        self._last_switch = switch.index
        lane = self._path[(switch.index + 1) % len(self._path)]
        if len(self._lane_switching) > 0:
            future_lane = self._lane_switching.pop(0)
        else:
            future_lane = self.lane[1]
        if abs(self.lane[1] - lane) > 1:
            lane = self.lane[1] + (lane - self.lane[1])/abs((lane - self.lane[1]))
        self._lane_switching.append(lane)
        if lane == future_lane:
            logger.debug('[LANE] IGNORE lane:%s future:%s' % (switch.index, future_lane))
            return
        elif lane < future_lane:
            logger.debug('[LANE] Queue change LEFT lane:%s future:%s' % (switch.index, future_lane))
            self._lane_switch.append((switch.index, 'Left'))
        elif lane > future_lane:
            logger.debug('[LANE] Queue change RIGHT lane:%s future:%s' % (switch.index, future_lane))
            self._lane_switch.append((switch.index, 'Right'))
    
    def can_update_lane(self):
        if len(self._lane_switch) > 0:
            l = self._lane_switch[0]
            if abs(self.angle) < 6:
                return True
        return False
    
    def _update_throttle(self):
        # Setup
        pos = self._position[-1] + (0,)
        self.next_piece_dist = self.race.track.compute_distance(pos, (self.piece_index(1), 0, 0))
        self.next_piece = self.piece(1)
        piece = self.piece(0)

        # Default throttle
        value = 1.0

        self._max_angle = max(self.angle, self._max_angle)

        self.next_bend, self.next_bend_dist = self._find_next_bend()
        
        if piece.is_bend():
            self._bend_data.append((self.speed, self.acceleration, self.angle))
        
        # don't process next bend until current one has been survived...
        #if piece.is_bend() and self.next_bend_dist > 30:
        #    self.next_bend = piece

        self.target_speed = self._compute_bend_speed()
        self.next_bend_braking_dist = self._compute_braking_distance()

        # Throttle control for in-bend
        if piece.is_bend() and self.speed > self.target_speed:
            value = 0.0

        # Throttle control
        if (self.next_bend_braking_dist+self.speed*(max(0.01, self.acceleration))) >= self.next_bend_dist+20:
            value = 0.0

        #if self.next_bend_dist < 25 and self.angle < 20:
        #    value = 1.0

        if abs(self.angle) > 57:
            logger.info('[WARNING] car slip angle too great: %7.3f pos:%s' % (self.angle, pos))

        if (45 < self.angle < 49.0):
            value = 0.0
        if (self.angle > 54.0):
            value = 0.0

        # Append new speed
        self._throttle.append(min(max(value, 0.0), 1.0))

    def _find_next_bend(self):
        next_bend = None
        distance_to_bend = self.next_piece_dist or 0.0
        for i in range(1, 60):
            p = self.piece(i)
            if p.is_bend():
                next_bend = p
                break
            distance_to_bend += self.race.track.get_lane_length(i, self._path[p.index])
        return next_bend, distance_to_bend


    def other_car_distance(self):
        abs_min_dist = 10000.0
        min_dist = 10000.0
        for color, car in self.race.cars.iteritems():
            pos = car.position
            if not pos:
                continue
            if color == self.color:
                continue
            d = self.race.track.compute_distance(self.position + (self.lane[0],), pos + (car.lane[0],))
            abs_d = abs(d)
            if d < abs_min_dist:
                abs_min_dist = abs(d)
                min_dist = d
        return min_dist

    def _approx_acceleration(self):
        value = 0.1075
        if len(self._acceleration) < 4:
            return value
            
        if self._accel is not None:
            return self._accel

        v = np.array([self._speed[-4], self._speed[-3], self._speed[-2],self._speed[-1]])
        if not np.all(v[1:] > 0):
            return value
        if v[3]-v[2] < 0:
            return value

        if abs(self.other_car_distance()) < 10:
            return value

        h = 1.0
        v = np.array([self._speed[-4], self._speed[-3], self._speed[-2], self._speed[-1]])
        pos1 = self._position[-1]
        pos2 = self._position[-2]
        if len(pos1) == 2:
            pos1 += (0, )
        if len(pos2) == 2:
            pos2 += (0, )
        d = self.race.track.compute_distance(pos2, pos1)
        k = (v[1] - (v[2] - v[1])) / v[1]**2
        if np.isnan(k) or np.isinf(k):
            return value
        self._accel = k
        logger.info('[PARAM] track accel: %7.4f' % (self._accel, ))
        return self._accel
        
    def _approx_braking_factor(self):
        value = 0.989
        if len(self._throttle) < 2:
            return value
        if self._brake is not None:
            return self._brake
        t = np.array([self._throttle[-2],self._throttle[-1]])
        if np.any(t > 0):
            return value
        v = np.array([self._speed[-1], self._speed[-2]])
        k = (v[0] - (v[1] - v[0])) / v[0]
        if k > 1.0:
            return value
        if np.isnan(k) or np.isinf(k):
            return value
        print 'distance to other cars', self.other_car_distance()
        if self.other_car_distance() < 10:
            return value
        logger.info('[PARAM] track deaccel: %7.4f' % (k, ))
        self._brake = k
        return k

    def _approx_mass(self):
        value = 6.24
        if self.other_car_distance() < 10:
            return value
        if len(self._speed) < 4 or self._speed[-2] <= 0.1:
            return value
        if self._mass is not None:
            return self._mass
        v = np.array([self._speed[-4], self._speed[-3], self._speed[-2],self._speed[-1]])
        if not np.all(v[1:] > 0):
            return value

        h = 1.0
        v = np.array([self._speed[-4], self._speed[-3], self._speed[-2],self._speed[-1]])
        # print v[-3], v[-2], v[-1]
        k = (v[1] - (v[2] - v[1])) / v[1]**2.0 * h
        m = 1.0 / (np.log((v[3] - (h / k)) / (v[2] - (h / k))) / (-k))
        if np.isnan(k) or np.isnan(m) or np.isinf(k) or np.isinf(m):
            return value
        if m < 0.01:
            return value
        self._mass = m
        logger.info('[PARAM] track mass: %7.4f' % (self._mass, ))
        return self._mass

    def _compute_bend_speed(self):
        mass = self._approx_mass()
        accel = self._approx_acceleration()
        radius = self.race.track.get_lane_radius(self.next_bend.index, self._path[self.next_bend.index])
        target_speed = mass * self._magic_bend_constant * np.sqrt(accel * radius)
        return target_speed

    def _compute_braking_distance(self):
        braking_factor = self._approx_braking_factor()
        v0 = self.speed
        n = sum((braking_factor ** np.arange(0, 400) * v0) >= self.target_speed) # ticks
        braking_dist = sum(braking_factor ** np.arange(0, n + 1) * v0)
        return braking_dist

    def update(self, data):
        super(MyCar, self).update(data)
        self._turbo_time = max(0, self._turbo_time - 1)


class Lane(object):
    def __init__(self, data):
        self.distance = data['distanceFromCenter']
        self.index = data['index']


class Piece(object):
    def __init__(self, data, index=None):
        self.length = data.get('length')
        self.radius = data.get('radius')
        self.angle = data.get('angle')
        self.switch = data.get('switch')
        self.index = index
    
    def is_straight(self):
        if self.radius is not None:
            return False
        return True
    
    def is_bend(self):
        if self.radius is not None:
            return True
        return False
    
    def has_switch(self):
        if self.switch is not None:
            return True
        return False


class Track(object):
    def __init__(self, data):
        self.name = data['name']
        self.id = data['id']
        self.pieces = [Piece(l, index) for index, l in enumerate(data['pieces'])]
        self.lanes = dict([(l['index'], Lane(l)) for l in data['lanes']])
    
    def get_lane_length(self, piece_idx, lane_idx):
        """Returns the length of a lane"""
        piece = self.pieces[piece_idx]
        lane = self.lanes[lane_idx]
        if piece.is_straight():
            return piece.length
        sign = piece.angle / abs(piece.angle)
        l = abs(piece.angle) * (piece.radius - sign * lane.distance) * np.pi / 180.0
        return l
    
    def get_lane_radius(self, piece_idx, lane_idx):
        piece = self.pieces[piece_idx]
        lane = self.lanes[lane_idx]
        if piece.is_straight():
            return 0.0
        sign = piece.angle / abs(piece.angle)
        return piece.radius - sign * lane.distance
    
    def get_longest_straight(self):
        
        data = {}
        start = None
        for piece in self.pieces:
            if piece.is_bend():
                start = None
                continue
            if not start:
                start = piece.index
                data[start] = piece.length
            else:
                data[start] += piece.length
        return sorted(list(data.iteritems()), reverse=True)[0]
            
            
    
    def compute_distance(self, start, end):
        if start[0] == end[0]:
            return end[1] - start[1]
        
        if start[2] == end[2]:
            dist = self.get_lane_length(start[0], start[2]) - start[1]
            dist += end[1]
            return dist
        return 100000.0
        # raise Exception("compute_distance not implemented")
    
    def shortest_path_lanes(self, initial_lane=0):
        dist = 0.0
        path = {}
        nodes = []
        for piece in self.pieces:
            if piece.has_switch():
                nodes.append(piece.index)
        
        g = Graph()
        node = nodes.pop(0)
        for lane0 in self.lanes:
            g.add_edge((0, initial_lane), (1, lane0), 0.0)
        # g.add_edge((0, initial_lane), (1, 1), 0.0)
        for lane0 in self.lanes:
            for i in range(1, node):
                lane_dist = self.get_lane_length(i, lane0)
                g.add_edge((i, lane0), (i+1, lane0), lane_dist)
        while nodes:
            to = nodes.pop(0)
            dist = 0.0
            for lane0 in self.lanes:
                for lane in self.lanes:
                    for i in range(0, to - node):
                        if i > 0:
                            lane_dist = self.get_lane_length(node+i, lane)
                            g.add_edge((node+i, lane), (node+i+1, lane), lane_dist)
                        else:
                            lane_dist = self.get_lane_length(node, lane0) / 2.0
                            lane_dist += self.get_lane_length(node, lane) / 2.0
                            g.add_edge((node, lane0), (node+1, lane), lane_dist)
            node = to
        last_index = sorted(self.pieces, key=lambda x: x.index, reverse=True)[0].index
        for lane0 in self.lanes:
            for i in range(node+1, len(self.pieces)):
                lane_dist = self.get_lane_length(i, lane0)
                g.add_edge((i-1, lane0), (i, lane0), 0.0)
                last_index = i
    
        for lane0 in self.lanes:
            g.add_edge((last_index, lane0), (last_index+1, 0), 0.0)
    
        path = g.shortest_path((0, initial_lane), (last_index+1, 0))[:-1]
        return dict(path)

class Edge(object):
    def __init__(self, node, to, weight=None):
        self.node = node
        self.to = to
        self.weight = weight

class Graph(object):
    def __init__(self):
        self._out_edges = {}
    
    def add_edge(self, node, to, weight=None):

        edges = self._out_edges.setdefault(node, [])
        edge = (to, weight or 0.0)
        if edge not in edges:
            # print 'add edge', node, '->', to
            self._out_edges.setdefault(to, [])
            edges.append(edge)
    
    def edges(self, node):
        return self._out_edges.get(node, [])
    
    def _init(self, s):
        self._dist = {}
        self._pi = {}
        for v in self._out_edges.keys():
            self._dist[v] = 99999.0
            self._pi[v] = None
        self._dist[s] = 0
    
    def _relax(self, u, v, w):
        if (self._dist[v] >= self._dist[u] + w):
            self._dist[v] = self._dist[u] + w
            self._pi[v] = u
    
    def shortest_path(self, src, dst):
        self._init(src)
        keys = self._out_edges.keys()
        keys = sorted(keys)

        for u in keys:
            # print 'sort', sorted(self.edges(u))
            for v,w in sorted(self.edges(u)):
                self._relax(u, v, w)
        path = []
        n = dst
        while n:
            path.append(n)
            n = self._pi[n]
        
        return path[::-1]



class Race(object):
    def __init__(self, data):
        self.cars = {}
        self._race = data['race']
        self.gameId = None
        self.tick = 0
        self.track = Track(self._race['track'])
        self.my_car_color = None
        self.my_car_cls = Car
        
        self._raceSession = self._race.get('raceSession', {})
        self.max_laps = self._raceSession.get('laps')
        self.max_lap_time = self._raceSession.get('maxLapTimeMs')
        self.quickrace = self._raceSession.get('quickRace')
        self.finished = False
        self.setup_cars()
    
    def reset(self):
        logger.info('Race reset')
        self.tick = 0
        for car in self.cars.values():
            car.on_game_reset()

    def on_game_end(self):
        self.tick = None

    def setup_cars(self):
        self.cars = {}
        for entry in self._race['cars']:
            car_id = entry['id']
            if self.my_car_color and car_id['color'] == self.my_car_color:
                car = self.my_car_cls(self, car_id['name'], car_id['color'])
            else:
                car = Car(self, car_id['name'], car_id['color'])
            self.cars[car.color] = car


class RaceAI(object):
    pass


class RaceClient(object):
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = unicode(name)
        self.color = None
        self.key = key
        self.race = None
        self.trackname = None
        self.car_count = None
        self.password = None
    
    def msg(self, msg_type, data, tick=False):
        msg_data = {"msgType": msg_type, "data": data}
        if tick and self.race and self.race.tick is not None:
            msg_data.update({'gameTick': self.race.tick})
        msg = json.dumps(msg_data)
        # logger.debug('[SEND] %s' % (msg, ))
        self.send(msg)
    
    def send(self, msg):
        self.socket.sendall(msg + "\n")
    
    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, trackname=None, car_count=None, password=None):
        data = {
            "botId": {
                "name": self.name,
                 "key": self.key},
        }
        if car_count:
            data['carCount'] = car_count
        if trackname:
            data['trackName'] = trackname
        if password:
            data['password'] = password
        print 'data', data
        return self.msg("joinRace", data)
    

    def throttle(self, throttle):
        self._throttle = throttle
        self.msg("throttle", throttle, tick=True)
    
    def ping(self):
        self.msg("ping", {}, tick=False)
    
    def switch_lane(self, direction):
        self.msg('switchLane', direction.title())
    
    def run(self):
        if self.trackname or self.car_count:
            self.join_race(self.trackname, car_count=self.car_count, password=self.password)
        else: 
            self.join()

        self.msg_loop()
    
    def on_join_race(self, data):
        logger.debug("[EVENT] Joined race (joinRace) %s" % (data, ))
        self.ping()
    
    def on_join(self, data):
        logger.debug("[EVENT] Joined")
        self.ping()
    
    def on_your_car(self, data):
        logger.debug("[EVENT] yourCar: %s" % (data, ))
        self.color = data['color']
        self.ping()
    
    def on_error(self, data):
        logger.debug("[ERROR]: {0}".format(data))
        self.ping()
    
    def on_game_init(self, data):
        logger.info('[EVENT] gameInit: %s' % (data, ))
        self.race = Race(data)
        self.finished = False
        self.ping()
    
    def on_game_start(self, data):
        logger.debug("[EVENT] Race started")
        self.ping()
    
    def on_car_positions(self, data):
        self.ping()
    
    def on_lap_finished(self, data):
        logger.info('[EVENT] lap finished: %s' % (data, ))
        self.ping()
    
    def on_crash(self, data):
        logger.info("[EVENT] Car crashed: %s" % (data['name'], ))
        self.ping()
    
    def on_spawn(self, data):
        logger.info("[EVENT] Car spawned: %s" % (data['name'], ))
        self.ping()
    
    def on_game_end(self, data):
        logger.info("[EVENT] Race ended")
        if self.race:
            self.race.on_game_end()
        self.ping()
    
    def on_tournament_end(self, data):
        logger.info("[EVENT] Tournament ended")
        if self.race:
            self.race.finished = True
        self.ping()
        
    def on_finish(self, data):
        logger.info('[EVENT] Finish')
        print 'FINISH', data
        if self.race:
            car = self.race.cars.get(data['color'])
            if car:
                car.finished = True
        self.ping()
    
    def on_turbo_available(self, data):
        logger.debug('[EVENT] Turbo available')
        self.ping()
    
    def msg_loop(self):
        self.socket_file = self.socket.makefile()
        try:
            self._msg_loop()
        except Exception, e:
            print 'Exception! %s' % (e, )
            logger.exception('Exception')
        finally:
            logger.info('Closing connection')
            self.socket_file.close()
            logger.info('All done. okthnxbye.')
    
    def _msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'yourCar': self.on_your_car,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'lapFinished': self.on_lap_finished,
            'finish': self.on_finish,
            'carPositions': self.on_car_positions,
            'tournamentEnd': self.on_tournament_end,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        
        line = self.socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if self.race:
                self.race.tick = msg.get('gameTick', self.race.tick)
                self.race.gameId = msg.get('gameId', self.race.gameId)
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                logger.info("Got {0}".format(msg_type))
                self.ping()
            line = self.socket_file.readline()


class RaceBot(RaceClient):
    send_telemetry = False
    def on_crash(self, data):
        logger.info("[EVENT] Car crashed: %s" % (data['name'], ))
        self.race.cars[data['color']].crashed = True
        self.ping()
    
    def on_spawn(self, data):
        logger.info("[EVENT] Car spawned: %s" % (data['name'], ))
        self.race.cars[data['color']].crashed = False
        self.ping()
    
    def on_game_init(self, data):
        logger.info('[EVENT] gameInit: %s' % (data, ))
        if self.race:
            self.race.reset()
        else:
            self.race = Race(data)
            self.race.my_car_cls = MyCar
            self.race.my_car_color = self.color
            self.race.setup_cars()
            
        if self.send_telemetry:
            self.telemetry = Telemetry(self.race.gameId)
        else:
            self.telemetry = None
        self.ping()

    def update_car_lane(self, my_car):
        lane = my_car.get_next_lane()
        if lane:
            logger.debug('[LANE] Sent switchLane %s:%s' % (lane[0], lane[1], ))
            self.switch_lane(lane[1])
    
    
    def on_turbo_available(self, data):
        logger.debug('[EVENT] Turbo available')
        my_car = self.race.cars.get(self.color)
        my_car.turbo_available = data
        self.ping()
    
    def on_car_positions(self, data):
        if not (self.race and self.race.tick):
            logger.debug('Not updating on carPositions. Sending ping...')
            return self.ping()
        if (self.race and self.race.finished):
            logger.debug('Not updating on carPositions. Race has finished.')
            return self.ping()

        proctime = time.time()
        for state in data:
            name = unicode(state['id']['name'])
            color = unicode(state['id']['color'])
            car = self.race.cars.get(color)
            car.update(state)
            msg_params = {
                'tick': self.race.tick,
                'color': car.color,
                'lap': car.lap,
                'throttle': car.throttle,
                'speed': car.speed,
                'angle': car.angle,
                'piece': car.position[0],
                'inpiece': car.position[1],
                'piece_angle': car.piece(0).angle or 0,
                'piece_radius': car.piece(0).radius or 0,
                'dist': car.position[1],
                'lane': car.lane[0],
                'force': car.force,
                'acceleration': car.acceleration,
                'angular_speed': car.angular_speed,
            }
            if self.send_telemetry:
                self.telemetry.send(msg_params)

        if not self.color:
            logger.debug('on_car_position -> ping')
            return self.ping()
        
        my_car = self.race.cars.get(self.color)
        proctime = time.time() - proctime

        if my_car.can_update_lane():
            self.update_car_lane(my_car)
        elif my_car.should_turbo():
            my_car.use_turbo()
            self.msg('turbo', 'pow!')
        else:
            self.throttle(my_car.throttle)
        
        msg_params = {
            'tick': self.race.tick,
            'color': my_car.color,
            'lap': my_car.lap,
            'throttle': my_car.throttle,
            'speed': my_car.speed,
            'angle': my_car.angle,
            'piece': my_car.position[0],
            'piece_angle': car.piece(0).angle or 0,
            'dist': my_car.position[1],
            'lane': my_car.lane[0],
            'acceleration': my_car.acceleration,
            'target_speed': my_car.target_speed,
            'next_bend_dist': my_car.next_bend_dist,
        }
        msg = '[STATUS] tick:%(tick)5d lap:%(lap)2d lane:%(lane)1d piece:%(piece)2d:%(dist)06.3f:angle:%(piece_angle)07.3f' \
              ' throttle:%(throttle)03.2f speed:%(speed)06.3f target_speed:%(target_speed)06.3f angle:%(angle)07.3f' \
              ' accel:%(acceleration)07.3f next_bend_dist:%(next_bend_dist)07.3f' % msg_params
        if not my_car.crashed:
            logger.debug(msg)
