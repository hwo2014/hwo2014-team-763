import json
import socket
import sys

import logging
import logging.config
logger = logging.getLogger('racecondition.main')
logging.config.fileConfig('logging.ini')


import racebot

if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Usage: ./join_race.py host port botname botkey trackname")
    else:
        host, port, name, key, trackname = sys.argv[1:6]
        logger.info('Connecting with parameters:')
        logger.info("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = racebot.RaceBot(s, name, key)
        bot.trackname = trackname
        bot.car_count = 4
        bot.password = 'racecondition'
        print 'run'
        bot.run()